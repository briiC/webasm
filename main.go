package main

import (
	"log"
	"net/http"
	"strings"
)

func main() {

	webdir := "./web/"
	listenAddr := "localhost:3000"

	fs := http.FileServer(http.Dir(webdir))
	log.Printf("Serving [ %s ] on [ %s ]", webdir, listenAddr)
	http.ListenAndServe(listenAddr, http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		resp.Header().Add("Cache-Control", "no-cache")

		if strings.HasSuffix(req.URL.Path, ".wasm") {
			resp.Header().Set("content-type", "application/wasm")
		}

		fs.ServeHTTP(resp, req)
	}))
}
