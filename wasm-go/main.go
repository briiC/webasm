package main

import (
	"fmt"
	"log"
	"syscall/js"
	"time"
)

func main() {
	println("WASM (Go) initialized (./wasm-go/main.go)")
	doc := js.Global().Get("document")

	// getElements := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
	// 	for i, el := range args {
	// 		println(i)
	// 		log.Println(el)
	// 	}
	//
	// 	return nil
	// })
	// js.Global().Set("getElements", getElements)

	els := js.Global().Get("elementsByClass")
	resp := els.Invoke("progress")
	log.Printf("%+v", resp)

	// alert := js.Global().Get("alert")
	// alert.Invoke("Go WASM Here!")

	go showUsers()

	// js.Global().Get("document").Call("getElementById", "myButton").Call("addEventListener", "click", cb)

	go progressbarTest()

	btnToggleLeft := doc.Call("getElementById", "btnToggleLeft")
	btnToggleRight := doc.Call("getElementById", "btnToggleRight")

	time.Sleep(1 * time.Second)
	log.Printf("Open LEFT")
	btnToggleLeft.Call("click")

	time.Sleep(1 * time.Second)
	log.Printf("Open RIGHT")
	btnToggleRight.Call("click")

	time.Sleep(1 * time.Second)
	log.Printf("Close both")
	btnToggleLeft.Call("click")
	btnToggleRight.Call("click")

	// make sure not exit to allow background processes to work
	for {
		select {}
	}
}

//
func showUsers() {
	doc := js.Global().Get("document")
	divLastUser := doc.Call("getElementById", "last-user-added")
	divUsers := doc.Call("getElementById", "users")

	for i := 0; i < 10; i++ {
		user := NewUser()
		log.Printf("User: %s", user)

		// Set last user added in container
		divLastUser.Set("innerHTML", user.Render())

		// Append to all users
		el := doc.Call("createElement", "div")
		el.Set("innerHTML", user.Render())
		divUsers.Call("appendChild", el)

		time.Sleep(1 * time.Second)
	}
}

//
func progressbarTest() {
	doc := js.Global().Get("document")

	el := doc.Call("getElementById", "progressbar")
	el.Call("setAttribute", "style", "border:0.2em solid pink;")
	log.Printf(".progressbar: %T --  %+v", el, el)

	pbar := doc.Call("getElementById", "progress-bar")
	log.Printf(".progressbar: %T --  %+v", pbar, pbar)

	for i := 0; i <= 100; i += 20 {
		style := fmt.Sprintf("width: %d%%;", i)
		pbar.Call("setAttribute", "style", style)
		time.Sleep(1 * time.Second)
	}

	// all progreessbars
	progressbars := doc.Call("getElementsByClassName", "progress-bar")
	log.Printf("getElementsByClassName(): %+v", progressbars)
	for perc := 0; perc <= 100; perc += 10 {

		for i := 0; ; i++ {
			pbar := progressbars.Index(i)

			if pbar == js.Undefined() {
				break
			}

			log.Printf(".Index(%d): %s", i, pbar)

			style := fmt.Sprintf("width: %d%%;", perc)
			pbar.Call("setAttribute", "style", style)
		}

		time.Sleep(250 * time.Millisecond)
	}

}
