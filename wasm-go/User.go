package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"time"

	"github.com/Pallinder/go-randomdata"
)

// User ..
type User struct {
	ID          int
	Username    string
	FullName    string
	Address     string
	TimeUpdated time.Time
}

const templateUser string = `
<div class="user card bg-dark text-white m-3" id="user-{{.ID}}" data-id="{{.ID}}" onclick="">
  <div class="card-header">
    <h4><code>#{{.ID}}</code> &bull; {{.FullName}}</h4>
  </div>
  <div class="card-body">
    <h5 class="card-title">a.k.a. <code>{{.Username}}</code></h5>
    <p class="card-text">{{.Address}}</p>
    <small>Time updated: {{.TimeUpdated}}</small>
  </div>
</div>
`

// NewUser - random user
func NewUser() *User {
	u := &User{
		ID:          randomdata.Number(100, 10000),
		Username:    randomdata.SillyName(),
		FullName:    randomdata.FullName(randomdata.RandomGender),
		Address:     randomdata.Address(),
		TimeUpdated: time.Now(),
	}
	return u
}

// String - show human readable output for User struct
func (user *User) String() string {
	s := "[User]"
	s += fmt.Sprintf(" #%-5d", user.ID)
	s += user.FullName
	s += " (" + user.Username + ")"
	return s
}

// Render - HTML output for user
func (user *User) Render() string {
	tmpl, err := template.New("user").Parse(templateUser)
	if err != nil {
		log.Printf("ERROR: %s", err)
		return "ERROR!"
	}

	buf := bytes.NewBuffer([]byte{})
	if err := tmpl.Execute(buf, user); err != nil {
		log.Printf("ERROR: %s", err)
		return "ERROR!"
	}

	log.Printf("Render: User [%s]", buf)
	return string(buf.Bytes())
}
