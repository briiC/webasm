from fabric.api import *
from fabric.colors import *


@task
def build_wasm():
    """Build/Rebuild WASM packages
    """

    print yellow("# Copy wasm_exec.js..")
    local('cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./web/wasm_exec.js')

    print yellow("# Building (wasm)..")
    local("GOOS=js GOARCH=wasm go build -o ./web/main.wasm ./wasm-go/*.go")
    local("tree -D -tr")


@task
def build_webserver():
    """Build/Rebuild Webserver (go)
    """

    print yellow("# Building (webserver)..")
    local("go build -o webserver")
    local("tree -D -tr")


@task
def localrun():
    """Run locally
    """

    build_wasm()
    build_webserver()

    print yellow("# Run..")
    local("./webserver")
